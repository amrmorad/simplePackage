Package.describe({
  name: 'amr:rocketchat-notifications',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'Hello notification after signing in',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4.2.3');
  api.use([
    'ecmascript',
    'templating',
    'kadira:flow-router'
  ]);

  api.addFiles([
    'client/route.js',
    'client/rocketchat-notifications.js'
    ], 'client');
}); 

Package.onTest(function(api) {
  
});
